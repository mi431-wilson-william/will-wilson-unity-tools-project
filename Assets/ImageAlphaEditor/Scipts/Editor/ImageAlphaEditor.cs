using System;
using System.IO;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using Object = UnityEngine.Object;

namespace WW
{
    public class ImageAlphaEditor : EditorWindow
    {
        private ObjectField textureField;
        private DropdownField alphaDropdown;
        private GradientField alphaGradient;
        private VisualElement imagePreview;
        private SliderInt alphaSlider;
        private Button exportButton;
        private string outputName;
        private Texture2D selectedTexture;
        private Texture2D outputTexture;
        private VisualElement customTexValues;
        private DropdownField textureOption;
        private IntegerField widthField;
        private IntegerField heightField;
        private Button createTexButton;
        private ColorField tint;
        private ComputeShader shader;
        private IntegerField alphaInput;


        [MenuItem("Tools/Image Alpha Editor")]
        public static void OpenEditorWindow()
        {
            ImageAlphaEditor wnd = GetWindow<ImageAlphaEditor>();
            wnd.titleContent = new GUIContent("Image Alpha Editor");
            wnd.maxSize = new Vector2(320, 500);
            wnd.minSize = wnd.maxSize;

        }

        private void CreateGUI()
        {
            shader = AssetDatabase.LoadAssetAtPath<ComputeShader>("Assets/ImageAlphaEditor/Scipts/AlphaEdit.compute");
            VisualElement root = rootVisualElement;
            var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/ImageAlphaEditor/Resources/UIDocuments/ImageAlphaEditorWindow.uxml");
            VisualElement tree = visualTree.Instantiate();
            root.Add(tree);

            //assign elements
            textureField = root.Q<ObjectField>("texture-field");
            alphaDropdown = root.Q<DropdownField>("alpha-dropdown");
            textureOption = root.Q<DropdownField>("texture-option");
            alphaGradient = root.Q<GradientField>();
            imagePreview = root.Q<VisualElement>("image-preview");
            customTexValues = root.Q<VisualElement>("custom-tex-values");
            alphaSlider = root.Q<SliderInt>();
            exportButton = root.Q<Button>("export-button");
            createTexButton = root.Q<Button>("create-tex-button");
            widthField = root.Q<IntegerField>("width-field");
            heightField = root.Q<IntegerField>("height-field");
            tint = root.Q<ColorField>("tint");
            alphaInput = root.Q<IntegerField>("alpha-input");

            //Assign Callbacks
            //Whenever a value changes, it invokes these callback funcs
            textureField.RegisterValueChangedCallback<Object>(TextureSelected);
            alphaDropdown.RegisterValueChangedCallback<string>(AlphaOptionSelected);
            textureOption.RegisterValueChangedCallback<string>(TextureOptionSelected);
            alphaSlider.RegisterValueChangedCallback<int>(AlphaSliderChanged);
            alphaInput.RegisterValueChangedCallback<int>(AlphaInputChanged);
            alphaGradient.RegisterValueChangedCallback<Gradient>(AlphaGradientChanged);
            tint.RegisterValueChangedCallback<Color>(TintChanged);
            exportButton.clicked += () => ExportImage(outputTexture);
            createTexButton.clicked += CreateTexture;

            imagePreview.style.backgroundImage = null;
            TextureOptionSelected(null);
            AlphaOptionSelected(null);
        }

        #region Event Callback
        private void TextureSelected(ChangeEvent<Object> evt)
        {
            //if there is no texture selected, null the preview
            if(evt.newValue == null)
            {
                selectedTexture = null;
                imagePreview.style.backgroundImage = null;
                return;
            }
            //Assign the given texture as the selected texture
            outputName = evt.newValue.name + "Adjusted";
            selectedTexture = evt.newValue as Texture2D;
            SetPreviewDimensions(selectedTexture.width, selectedTexture.height);
            ApplyAlphaGradient();
        }

        private void AlphaOptionSelected(ChangeEvent<string> evt)
        {
            //hide and show things based on drop down selection
            if (alphaDropdown.value != alphaDropdown.choices[0])
            {
                alphaSlider.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.None);
                alphaGradient.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.Flex);
            }
            else
            {
                alphaSlider.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.Flex);
                alphaGradient.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.None);
            }

            ApplyAlphaGradient();
        }

        private void TextureOptionSelected(ChangeEvent<string> evt)
        {
            //hide and show things based on drop down selection
            if (textureOption.value != textureOption.choices[0])
            {
                textureField.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.Flex);
                customTexValues.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.None);
            }
            else
            {
                textureField.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.None);
                customTexValues.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.Flex);
            }

            selectedTexture = null;
            textureField.value = null;
            imagePreview.style.backgroundImage = null;
            ApplyAlphaGradient();
        }

        private void AlphaSliderChanged(ChangeEvent<int> evt)
        {
            alphaInput.SetValueWithoutNotify(evt.newValue);
            ApplyAlphaGradient();
        }

        private void AlphaInputChanged(ChangeEvent<int> evt)
        {
            alphaSlider.SetValueWithoutNotify(evt.newValue);
            ApplyAlphaGradient();
        }

        private void AlphaGradientChanged(ChangeEvent<Gradient> evt)
        {
            ApplyAlphaGradient();
        }

        private void TintChanged(ChangeEvent<Color> evt)
        {
            ApplyAlphaGradient();
        }
        #endregion

        #region Button Methods
        //export the image as a png, only works in editor
        private void ExportImage(Texture2D texture2D)
        {
            //call saveFilePanel method to begin the save
            var path = EditorUtility.SaveFilePanel(
                "Save Edited Texture",
                Application.dataPath,
                outputName + ".png",
                "png");
            byte[] bytes = texture2D.EncodeToPNG();
            if (string.IsNullOrEmpty(path))
            {
                return;
            }
            File.WriteAllBytes(path, bytes);

            string pathString = path;
            //find the index of assets in pathString
            int assetIndex = pathString.IndexOf("Assets", StringComparison.Ordinal);
            //create a substring from that index
            string filePath = pathString.Substring(assetIndex, path.Length - assetIndex);
            AssetDatabase.ImportAsset(filePath);
            AssetDatabase.Refresh();
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = (Texture2D)AssetDatabase.LoadAssetAtPath(filePath, typeof(Texture2D));
            Debug.Log("filepath is " + filePath);
        }
        
        
        private void CreateTexture()
        {
            //create a white texture
            int texWidth = widthField.value;
            int texHeight = heightField.value;
            selectedTexture = new Texture2D(texWidth, texHeight, TextureFormat.RGBA32, false);
            for(int y = 0; y < texHeight; y++)
            {
                for(int x = 0; x < texWidth; x++)
                {
                    selectedTexture.SetPixel(x, y, Color.white);
                }
            }
            selectedTexture.Apply();
            outputName = "CustomTex";

            //scale the preview image
            SetPreviewDimensions(texWidth, texHeight);

            ApplyAlphaGradient();
        }

        //scale the preview based on the dimensions of the texture
        private void SetPreviewDimensions(int texWidth, int texHeight)
        {
            bool greaterWidth = (texWidth > texHeight);
            float xRatio = 1;
            float yRatio = 1;
            if (greaterWidth)
            {
                yRatio = (float)texHeight / (float)texWidth;
            }
            else
            {
                xRatio = (float)texWidth / (float)texHeight;
            }
            imagePreview.style.width = 300 * xRatio;
            imagePreview.style.height = 300 * yRatio;
        }
        #endregion

        #region Alpha Methods
        private void ApplyAlphaGradient()
        {
            //if there is no selected texture, unenable the export
            if(selectedTexture == null)
            {
                exportButton.SetEnabled(false);
                return;
            }

            exportButton.SetEnabled(true);

            //create a new texture with the same dimensions as selectedTexture
            outputTexture = new Texture2D(selectedTexture.width, selectedTexture.height, TextureFormat.RGBA32, false);

            //depending on option, call method to augment the selected texture
            switch (alphaDropdown.index)
            {
                case 0:
                    outputTexture = AlphaWhole();
                    break;
                case 1:
                    outputTexture = GradientRight();
                    break;
                case 2:
                    outputTexture = GradientLeft();
                    break;
                case 3:
                    outputTexture = GradientBottom();
                    break;
                case 4:
                    outputTexture = GradientTop();
                    break;
            }

            //display new texture on preview
            imagePreview.style.backgroundImage = outputTexture;
        }
        
        private Texture2D AlphaWhole()
        {
            float alpha = (float)alphaSlider.value / 255f;
            return TextureOpacity(alpha);
        }

        private Texture2D GradientRight()
        {
            throw new NotImplementedException();
        }

        private Texture2D GradientLeft()
        {
            throw new NotImplementedException();
        }

        private Texture2D GradientBottom()
        {
            throw new NotImplementedException();
        }

        private Texture2D GradientTop()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Compute Shader Methods
        //This prevents errors relating to the texture size 
        private int SetRunCount(int dimensionSize)
        {
            int count = dimensionSize / 8;
            if(dimensionSize % 8 > 0)
            {
                count++;
            }
            return count;
        }
        private Texture2D TextureOpacity(float alpha)
        {
            //create kernel handle to save method of alphawhole
            int kernelHandle = shader.FindKernel("AlphaWhole");

            //create new texture with same dimensions as selected texture
            Texture2D selectedTex = new Texture2D(selectedTexture.width, selectedTexture.height,
                TextureFormat.RGBA32, false);

            //create two render textures to pass to shader.
            //resultTex will contain result of shader
            RenderTexture resultTex = new RenderTexture(selectedTexture.width, selectedTexture.height,
                32);
            resultTex.enableRandomWrite = true;
            resultTex.Create();

            //inputTex is the texture supplied to shader.
            RenderTexture inputTex = new RenderTexture(selectedTexture.width,
                selectedTexture.height, 32);
            inputTex.enableRandomWrite = true;
            inputTex.Create();

            //set inputTex as active render texture
            RenderTexture.active = inputTex;
            Graphics.Blit(selectedTexture, inputTex);

            //assign inputTex to the input texture property
            shader.SetTexture(kernelHandle, "Input", inputTex);

            //set resultTex as active render texture, and as the result of the compute shader
            RenderTexture.active = resultTex;
            shader.SetTexture(kernelHandle, "Result", resultTex);

            //set the opacity to the alpha float, and float4 tint as the value of the tint editor window
            shader.SetFloat("opacity", alpha);
            shader.SetVector("tint", tint.value);

            //set threadgroups 
            shader.Dispatch(kernelHandle, SetRunCount(resultTex.width), SetRunCount(resultTex.height), 1);

            //set pixels to be that of active render texture, which is resultTex
            selectedTex.ReadPixels(new Rect(0, 0, resultTex.width, resultTex.height), 0, 0);
            selectedTex.Apply();
            RenderTexture.active = null;
            return selectedTex;
        }

        #endregion
    }
}


