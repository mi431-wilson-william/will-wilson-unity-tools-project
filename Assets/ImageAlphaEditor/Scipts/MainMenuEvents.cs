using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class MainMenuEvents : MonoBehaviour
{
    private UIDocument _doc;

    //visual element containing all of the content of the page
    private VisualElement visElementCurrent; 

    //Main Menu Message
    private Label labelMainMessage;
    private Label labelStationStatus;
    private Label labelMessageStatus;

    //Buttons that lead to the different sub-menu's
    private Button navButtonName;
    private Button navButtonRadio;
    private Button navButtonComment;

    //Visual Tree Elements for the different sub-menus
    [SerializeField]
    private VisualTreeAsset pageNameTemplate;
    [SerializeField]
    private VisualTreeAsset pageRadioTemplate;
    [SerializeField]
    private VisualTreeAsset pageCommentTemplate;

    //Elements from the Visual Trees
    private VisualElement pageNameElements;
    private VisualElement pageRadioElements;
    private VisualElement pageCommentElements;

    //elements for the name page
    private Button nBack;
    private TextField nTextInput;

    //elements for the radio page
    private Button rBack;
    private SliderInt rSliderInput;

    //elements for the comment page
    private Button cBack;
    private TextField cTextInput;

    //source
    //https://www.youtube.com/watch?v=8w0qvO4Vumc

    void Awake()
    {
        //Grab the main menu doc and all relevent elements of the doc.
        _doc = GetComponent<UIDocument>();
        //main visual element container
        visElementCurrent = _doc.rootVisualElement.Q<VisualElement>("container");
        //labels
        labelMainMessage = _doc.rootVisualElement.Q<Label>("label-main-message");
        labelStationStatus = _doc.rootVisualElement.Q<Label>("label-station-status");
        labelMessageStatus = _doc.rootVisualElement.Q<Label>("label-message-status");

        //nav buttons
        navButtonName = _doc.rootVisualElement.Q<Button>("nav-button-name");
        navButtonRadio = _doc.rootVisualElement.Q<Button>("nav-button-radio");
        navButtonComment = _doc.rootVisualElement.Q<Button>("nav-button-comment");
        //clone each template to its respective VisualElement Counterpart
        pageNameElements = pageNameTemplate.CloneTree();
        pageRadioElements = pageRadioTemplate.CloneTree();
        pageCommentElements = pageCommentTemplate.CloneTree();

        //assign the back buttons for each subwindow
        nBack = pageNameElements.Q<Button>("nav-button-back");
        rBack = pageRadioElements.Q<Button>("nav-button-back");
        cBack = pageCommentElements.Q<Button>("nav-button-back");

        //input buttons for each subwindow
        nTextInput = pageNameElements.Q<TextField>("player-input");
        rSliderInput = pageRadioElements.Q<SliderInt>("signal-slider");
        cTextInput = pageCommentElements.Q<TextField>("player-input");

        //set the slider label
        rSliderInput.label = rSliderInput.value + " Hz";

        //click functions for each nav button
        navButtonName.clicked += NavButtonName_clicked;
        navButtonRadio.clicked += NavButtonRadio_clicked;
        navButtonComment.clicked += NavButtonComment_clicked;
        nBack.clicked += NavButtonBack_clicked;
        rBack.clicked += NavButtonBack_clicked;
        cBack.clicked += NavButtonBack_clicked;

        //callback functions
        nTextInput.RegisterCallback<ChangeEvent<string>>(NameChangeEvent);
        rSliderInput.RegisterCallback<ChangeEvent<int>>(RadioChangeEvent);
        cTextInput.RegisterCallback<ChangeEvent<string>>(CommentChangeEvent);
    }


    #region Callback
    private void NameChangeEvent(ChangeEvent<string> evt)
    {
        //Debug.Log(evt.newValue);
        //throw new NotImplementedException();
        labelMainMessage.text = "Welcome " + evt.newValue;
        //Debug.Log(labelMainMessage.text);
    }

    private void RadioChangeEvent(ChangeEvent<int> evt)
    {
        //Debug.Log(evt.newValue);
        rSliderInput.label = evt.newValue + " Hz";
        labelStationStatus.text = "broadcasting at " +evt.newValue +" Hz";
    }

    private void CommentChangeEvent(ChangeEvent<string> evt)
    {
        //Debug.Log(evt.newValue);
        labelMessageStatus.text = "message: " + evt.newValue;
    }
    #endregion

    #region OnClick
    private void NavButtonName_clicked()
    {
        visElementCurrent.Clear();
        visElementCurrent.Add(pageNameElements);
    }
    private void NavButtonRadio_clicked()
    {
        visElementCurrent.Clear();
        visElementCurrent.Add(pageRadioElements);
    }

    private void NavButtonComment_clicked()
    {
        visElementCurrent.Clear();
        visElementCurrent.Add(pageCommentElements);
    }

    //Behavior for the back button being clicked
    private void NavButtonBack_clicked()
    {
        visElementCurrent.Clear();
        visElementCurrent.Add(labelMainMessage);
        visElementCurrent.Add(navButtonName);
        visElementCurrent.Add(navButtonRadio);
        visElementCurrent.Add(navButtonComment);
        visElementCurrent.Add(labelStationStatus);
        visElementCurrent.Add(labelMessageStatus);
    }
    #endregion

}
